import moment from "moment";
import type { NextPage } from "next";
import DATA from "../DATA.json";

const Home: NextPage = () => {
  const today = new Date();
  const day = moment(today).format("D");
  const month = moment(today).format("M");
  const years = moment(today).format("YYYY");
  const startDay = moment([2022, 7, 10]);
  const toDay = moment([years, month, day]);
  const totalDay = toDay.diff(startDay, "days");
  const { text: quote, author } = DATA?.[totalDay];
  return (
    <div className="center">
      <p className="text">{quote}</p>
      <p className="author">
        {`_`}
        {author}
        {`_`}
      </p>
    </div>
  );
};

export default Home;
